from rest_framework.serializers import ModelSerializer

from .models import VehiculoSeminuevo


class VehiculoSeminuevoSerializer(ModelSerializer):
	class Meta:
		model = VehiculoSeminuevo
		fields = '__all__'

