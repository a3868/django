from django.apps import AppConfig


class InventarioSeminuevosConfig(AppConfig):
    name = 'inventario_seminuevos'
