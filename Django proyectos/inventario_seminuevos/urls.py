from django.urls import path
from .views import ListVehiculo, VehiculoSeminuevoDetail


urlpatterns = [
    path('seminuevos-inventario/', ListVehiculo.as_view()),
    path('seminuevos-inventario/<int:pk>/', VehiculoSeminuevoDetail.as_view())
]
