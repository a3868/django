from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import VehiculoSeminuevoSerializer
from .models import VehiculoSeminuevo

class ListVehiculo(APIView):
    
    def get(self, request):

        listado = VehiculoSeminuevoSerializer(
            VehiculoSeminuevo.objects.all(),many=True)
        return Response(listado.data)

    def post(self, request):

        vehiculo = VehiculoSeminuevoSerializer(data=request.data)

        if vehiculo.is_valid():
            vehiculo.save()
            return Response(vehiculo.data, status=201)

        return Response(vehiculo.errors, status=400)

class VehiculoSeminuevoDetail(APIView):

    def _get_object(self, pk):
        try:
            return VehiculoSeminuevo.objects.get(pk=pk)
        except VehiculoSeminuevo.DoesNotExist:
            raise Http404
    
    def get(self, request, pk):

        vehiculo = VehiculoSeminuevoSerializer(self._get_object(pk))
        return Response(vehiculo.data)

    def put(self, request, pk):
        vehiculo = VehiculoSeminuevoSerializer(self._get_object(pk), data=request.data)

        if vehiculo.is_valid():
            vehiculo.save()
            return Response(vehiculo.data)
        return Response(vehiculo.errors, status=400)


    def delete(self, request, pk):
        vehiculo = self._get_object(pk)
        vehiculo.delete()
        return Response(status=200)
