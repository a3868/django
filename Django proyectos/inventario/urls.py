
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('reporteinventario.urls')),
    path('api_v2/', include('inventario_seminuevos.urls'))
]

