from django.urls import path
from .views import ListInventario, VehiculoDetail


urlpatterns = [
    path('inventario/', ListInventario.as_view()),
    path('inventario/<int:pk>/', VehiculoDetail.as_view())
]
