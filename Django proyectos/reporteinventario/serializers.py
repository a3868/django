from rest_framework.serializers import ModelSerializer

from .models import Reporteinventario


class InventarioSerializer(ModelSerializer):
	class Meta:
		model = Reporteinventario
		fields = '__all__'

