from django.http import Http404
from rest_framework.views import APIView
from .models import Reporteinventario

from rest_framework.response import Response


from .serializers import InventarioSerializer


class ListInventario(APIView):
    
    def get(self, request):

        reporte = InventarioSerializer(
            Reporteinventario.objects.all(),many=True)
        return Response(reporte.data)

    def post(self, request):

        reporte = InventarioSerializer(data=request.data)

        if reporte.is_valid():
            reporte.save()
            return Response(reporte.data, status=201)

        return Response(reporte.errors, status=400)

class VehiculoDetail(APIView):
    """ Prospect details APIView
    """
    def _get_object(self, pk):
        """ _get_object
        Get a prospect information
        """
        try:
            return Reporteinventario.objects.get(pk=pk)
        except Reporteinventario.DoesNotExist:
            raise Http404
    
    def get(self, request, pk):

        vehiculo = InventarioSerializer(self._get_object(pk))
        return Response(vehiculo.data)

    def put(self, request, pk):
        vehiculo = InventarioSerializer(self._get_object(pk), data=request.data)

        if vehiculo.is_valid():
            vehiculo.save()
            return Response(vehiculo.data)
        return Response(vehiculo.errors, status=400)


    def delete(self, request, pk):
        vehiculo = self._get_object(pk)
        vehiculo.delete()
        return Response(status=200)
