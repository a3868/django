# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils.translation import ugettext as _


class Reporteinventario(models.Model):
    marca = models.CharField(max_length=100)  # Field name made lowercase.
    veh_tipoauto = models.CharField(max_length=60, blank=True, null=True)  # Field name made lowercase.
    veh_anmodelo = models.CharField(max_length=4, blank=True, null=True)  # Field name made lowercase.
    veh_kilometr = models.DecimalField(max_digits=18, decimal_places=0, blank=True, null=True)  # Field name made lowercase.
    veh_stransmision = models.CharField(max_length=30, blank=True, null=True)  # Field name made lowercase.
    veh_cilindros = models.CharField(max_length=25, blank=True, null=True)
    veh_coloexte = models.CharField(max_length=25, blank=True, null=True)  # Field name made lowercase.
    veh_colointe = models.CharField(max_length=25, blank=True, null=True)  # Field name made lowercase.
    equipamiento = models.CharField(max_length=1, blank=True, null=True)
    veh_jgollaves = models.CharField(max_length=2, blank=True, null=True)
    birlosseguridad = models.CharField(max_length=1, blank= True, null=True)
    llantaref = models.CharField(max_length=1, blank=True, null=True)
    gatosreflec = models.CharField(max_length=1, blank=True, null=True)
    veh_numserie = models.CharField(max_length=17, blank=True, null=True)  # Field name made lowercase.
    veh_noplacas = models.CharField(max_length=8, blank=True, null=True)  # Field name made lowercase.
    nombreenfact = models.CharField(max_length=1, blank=True, null=True)
    tipofactura = models.CharField(max_length=1, blank=True, null=True)
    veh_rangtenen = models.CharField(max_length=9, blank=True, null=True)
    veh_certverific = models.CharField(max_length=10, blank=True, null=True)
    multas = models.CharField(max_length=1, blank=True, null=True)
    referente = models.CharField(max_length=1, blank=True, null=True)
    valuador = models.CharField(max_length=1, blank=True, null=True)
    ubicasuc = models.CharField(max_length=1, blank=True, null=True)
    estimadocliente = models.CharField(max_length=1, blank=True, null=True)
    ofertavalu = models.CharField(max_length=1, blank=True, null=True)
    obsvalu = models.CharField(max_length=1, blank=True, null=True)
    tomaacuenta = models.CharField(max_length=5, blank=True, null=True)
    veh_sfecadqui = models.CharField(max_length=10, blank=True, null=True)  # Field name made lowercase.
    estatusdecompra = models.CharField(max_length=1, blank=True, null=True)
    tipoadquisicion = models.CharField(max_length=100, blank=True, null=True)
    veh_noinventa = models.DecimalField(max_digits=18, decimal_places=0, blank=True, null=True)
    antiguedad = models.IntegerField(blank=True, null=True)  # Field name made lowercase.
    veh_impfactplan = models.DecimalField(max_digits=18, decimal_places=5, blank=True, null=True)  # Field name made lowercase.
    veh_sctoacond = models.DecimalField(max_digits=18, decimal_places=5, blank=True, null=True)


    def __str__(self):
        return "MARCA: "f'{self.marca}-------------------VEHICULO:  {self.veh_tipoauto}'


    class Meta:
        verbose_name = _("Reporte")
        verbose_name_plural = _("Reporte")
