USE GMOFARRIL;
DROP table reporteinventario;

USE PROYECTO_DJANGO;
Delete reporteinventario_reporteinventario;
Drop table PROYECTO_DJANGO.dbo.TEMPORAL;
DBCC CHECKIDENT (reporteinventario_reporteinventario, RESEED, 0);

Use GMOFARRIL;
SELECT B.PAR_DESCRIP1 AS MARCA, VEH_TIPOAUTO,VEH_ANMODELO,VEH_KILOMETR, VEH_STRANSMISION, veh_cilindros, 
     VEH_COLOEXTE,VEH_COLOINTE, '' as equipamiento, veh_jgollaves, '' as birlosseguridad,'' as llantaref,'' as gatosreflec, 
        VEH_NUMSERIE,VEH_NOPLACAS, '' as nombreenfact,'' as tipofactura,veh_rangtenen,veh_certverific,'' as multas ,'' as referente,
       '' as valuador,
       '' as ubicasuc, '' as estimadocliente, '' as ofertavalu,'' as obsvalu,VEH_ACUENTA as tomaacuenta, VEH_SFECADQUI, 
        '' AS estatusdecompra, tipoadq.PAR_DESCRIP1 AS tipoadquisicion, 
        veh_noinventa, DATEDIFF(day,CONVERT(datetime,VEH_SFECADQUI,103),GETDATE()) AS ANTIGUEDAD,
       VEH_IMPFACTPLAN, VEH_SCTOACOND INTO reporteinventario FROM ser_vehiculo
  inner join pnc_parametr A on veh_situacion = a.par_idenpara 
  inner join pnc_parametr B on VEH_SMARCA = B.PAR_IDENPARA 
  inner join pnc_parametr C on VEH_STIPO = C.PAR_IDENPARA
  inner join pnc_parametr D on VEH_ORGUNIDAD = D.PAR_IDENPARA
  inner join pnc_parametr E on VEH_UBICACION = E.PAR_IDENPARA
  inner join pnc_parametr tipoadq on veh_stipadqui = tipoadq.par_idenpara 
----- el Where
WHERE A.PAR_TIPOPARA = 'SN' AND A.PAR_DESCRIP2 =  'SEMINUEVOS' AND B.PAR_TIPOPARA = 'MCAV' 
AND C.PAR_TIPOPARA = 'CVE'  AND D.PAR_TIPOPARA = 'OR' AND E.PAR_TIPOPARA = 'UBI' 
AND tipoadq.par_tipopara ='tadq' 
AND tipoadq.par_status = 'a'
AND veh_situacion in ('SFIS','SCONSIGNA','SSEP','CONSI')
----- el order by
ORDER BY VEH_NOINVENTA

Select * from reporteinventario;

SELECT * INTO PROYECTO_DJANGO.dbo.TEMPORAL FROM reporteinventario;

USE PROYECTO_DJANGO;
Insert into reporteinventario_reporteinventario Select * From TEMPORAL
Select * FROM reporteinventario_reporteinventario