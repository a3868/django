import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

import ListReporte from '@/components/Reporte/ListReporte'
import EditReporte from '@/components/Reporte/EditReporte'
import DeleteVehiculo from '@/components/Reporte/DeleteVehiculo'
import NewVehiculo from '@/components/Reporte/NewVehiculo'


import ListVehiculosSeminuevos from '@/components/Vehiculos_seminuevos/ListVehiculosSeminuevos'
import NewVehiculoSeminuevo from '@/components/Vehiculos_seminuevos/NewVehiculoSeminuevo'
import EditRegistroDeVehiculo from '@/components/Vehiculos_seminuevos/EditRegistroDeVehiculo'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/listado',
      name: 'ListReporte',
      component: ListReporte
    },
    {
      path: '/listado/editar/:vehiculoId',
      name: 'EditReporte',
      component: EditReporte
    },
    {
      path: '/vehiculos-seminuevos',
      name: 'ListVehiculosSeminuevos',
      component: ListVehiculosSeminuevos
    },
    {
      path: '/vehiculos-seminuevos/nuevo',
      name: 'NewVehiculoSeminuevo',
      component: NewVehiculoSeminuevo
    },
    {
      path: '/vehiculos-seminuevos/editar/:vehiculoId',
      name: 'EditRegistroDeVehiculo',
      component: EditRegistroDeVehiculo
    },
  ],
  mode: 'history'
})
